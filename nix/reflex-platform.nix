# $ nix-prefetch-url --unpack https://github.com/reflex-frp/reflex-platform/archive/develop.zip
let
  baseNixpkgs = import <nixpkgs> {};
  source = {
    reflex-platform = baseNixpkgs.fetchFromGitHub {
      owner = "reflex-frp";
      repo = "reflex-platform";
      rev = "510b990d0b11f0626afbec5fe8575b5b2395391b";
      sha256 = "09cmahsbxr0963wq171c7j139iyzz49hramr4v9nsf684wcwkngv";
    };
  };
  reflex-platform = import source.reflex-platform { system = builtins.currentSystem; };
in
  reflex-platform
